abstract class AppConstants {
  static const String appName = 'FunWithKanji';
  static const String appId = 'krillefear.funnykanji';
  static const String website = 'https://gitlab.com/KrilleFear/funny-kanji';
}
