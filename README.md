# Fun With Kanji

Simple Flutter app to learn Japanese writing systems Hiragana, Katakana and Kanji.

## Screenshots:

Not yet there.

## Install:

- [Latest APK from CI](https://gitlab.com/krillefear/funny-kanji/-/jobs/artifacts/main/browse?job=build_apk)

## Build:

Install [Flutter](https://flutter.dev) and run with:

```sh
flutter run
```